#!/usr/bin/env python3

files = open("tex-file.txt","rt")
file_data = files.read()
print(file_data)

#stip the blank line on the file
with open("ex-tex-file.txt","r") as files:
    lines = (line.rstrip() for line in files)
    lines = (line for line in lines if line)
    
    for no_blank_lines in lines:
        print(no_blank_lines)
    
