#!/usr/bin/env python3

#how use lambda
#Note: Not use lambda like this f = lambda x: 2*x
#for tracebacks is best def f(x): return 2*x

def myfunc(number):
    """This function multiplies for the number that you provide as a parameter.

    Parameters
    ----------
    number : int
        number who multiplied another give it as a parameter.
    Returns
    -------
    int
        Return the multiplication between another_number and the
        parameter number.
        
    """
    return lambda another_number : another_number * number

doubler = myfunc(2)
result = doubler(8)
print(f"The result is: {result}")

#this is another way to use it's the best way for tracebacks
def functions(x): return x*2
print(functions(2))
