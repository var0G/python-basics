#!/usr/bin/env python3

def add_function(number1, number2):
    """Receives two numbers and adds them up, then return the results

    Parameters
    ----------
    number1 : int 
        The first number to add.
    number2 : int
        The secod number to add.    

    Returns
    -------
    int
        Results by adding the two numbers.
        
    """
    results = number1 + number2
    return results


add = add_function(1, 2)
print(f"the result is : {add}\n")


