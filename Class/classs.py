#!/usr/bin/env python3

#Example to know how work class

class Chairclass:
    """The representation of a chair

    Attributes
    ----------
    color : str 
        The chair color.	
    price : int
        The chair price in USD.

    """
    def __init__(self):
        self.color = 'white'
        self.price = 40


class Person:
    """class for greet people

    Parameters
    ----------
    name : str
        The name of the person.
    age : int
        The age of the person. 

    Attributes
    ----------
    name : str
        The name of the person.	
    age : int
        The age of the person.

    """
    def __init__(self, name, age):
       self.name = name
       self.age = age

    def greet(self):
        """A simple method to say hi :v"""

        print("hi, my name is {} and i have {} years old".format(self.name,
                                                                self.age))

def main():
    objec_chair = Chairclass()
    print(objec_chair.color)
    
    #instancing he person
    person1 = Person("Jack", 40)
    person1.greet()   
    

main()
