#!/usr/bin/env python3

######
#way to print strings

name = "someName"
print("Hi "+name)

#with .format
pound = 18
print("Hi {}, give me $ {} pounds".format(name,pound)) 

#work to for numeric variables (the format :1.3f means one int and 3 floats)
frac = 78/89
print(" the fraction is {f:1.3f}".format(f=frac))

#f-strings
print(f"Hi {name} give me ${pound} , sorry i mean {frac:1.3f} cent")

print(" Hi %s give me %d, sorry i mean %.3f cent" % (name, pound, frac))
