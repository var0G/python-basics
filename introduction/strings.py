#!/usr/bin/env python3

#playing with strings
str_ing = "simple string"

#print the position 0 of the strings
print(str_ing[0],"position 0\n")

print("\nthe last position of the string is: ",str_ing[-1])

#here we can go from the 2 char to the 7 char of the string
print("\nfrom 2 to 7:", str_ing[2:7])

print("\nfrom 2 to the end of the string",str_ing[2:])

#split  the string and convert into a list 
print("\nspliting the string",str_ing.split())

#upper capitalize the string
for i in range(len(str_ing)):
    print(str_ing[i].upper())

