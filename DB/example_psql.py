#!/usr/bin/env python3
from dotenv import load_dotenv

secret_user = os.getenv('PSGUSER')
secret_pass = os.getenv('PSGPASS')

PGHOST = 'Localhost'
PGDATABASE = 'dbadmision'
PGUSER = secret_user
PGPASSWORD = secret_pass
