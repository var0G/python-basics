#!/usr/bin/env python3

import psycopg2
import sys
import os
import pandas as pd
import example_psql as creds

#**Set up a connection to the postgres server**#.

conn_string = "host="+ creds.PGHOST +" port="+ "5432" \
            +" dbname="+ creds.PGDATABASE +" user=" \
            + creds.PGUSER +" password="+ creds.PGPASSWORD

conn = psycopg2.connect(conn_string)
print("Conected Succesful!")

#cursor object
cursor = conn.cursor()

def load_data(schema, table):
    """This function load the data store in the 
    DB, the data depends on the Sql command.

    Parameters
    ----------
    schema : String
        The schema of the Data Base where is store the table
        that you need.
    table : String
        The table name that you need.    
    

    Returns
    -------
    String
        Returns the results of the sql statement
        
    """
    sql_command = "SELECT ri.nombres"\
                   +" FROM {}.{} ri".format(str(schema), str(table))\
                   +" WHERE ri.nombre='Ingeniería civil en informatica';"
                    
    print(sql_command)

    #load the data
    data = pd.read_sql(sql_command, conn)

    print(data.shape)
    return (data)

print(load_data('requerimiento_ad', 'ingresantes'))
