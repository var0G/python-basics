def count_positive_number(numbers):
    """Count only positive number on a given list

    Parameters
    ----------
    numbers : list
        list containing given numbers

    Returns
    -------
    list
        list with positive numbers
        
    """
    
    return [ num for num in numbers if num > 0 ]

def bool_positive(numbers):
    """return a Boolean true if is positive number

    Parameters
    ----------
    numbers : list
        list containing the number to inspect

    Returns
    -------
    list
        list with boolean true if is positive
        
    """
    return [ f"True:{num}" for num in numbers if num > 0 ]
    
test_numbers = [-1, -3, 23, 43, 23, -2, 3, 83]

print(count_positive_number(test_numbers))

print(bool_positive(test_numbers))
