#!/usr/bin/env python3

import re

#search examples

text = "this is  a example text"
print(re.search("example",text))

result = re.search("bye",text)

if(result):
    print("the string is in the text")
else:
    print("is not such string on text")


print(re.search("^this",text))

#findall examples

find_example = """
                his car is red,
                i don't have a red car,
                and you?
                """

print(re.findall("car.*red", find_example))

# split examples

split_examples = "my chair is withe and i spend 20 usd"

print(re.split("\s",split_examples))

#sub examples

sub_ex = "my chair is withe and i spend 20 usd"
print(re.sub("withe", "red",sub_ex))

